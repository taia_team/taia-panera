/**
 * Created by Cris on 17/11/2014.
 */
public class Missatge {

    public static final double SENSE_RESPOSTA = -1.0;

    private String _receiver;
    private String _sender;
    private Producte _product;
    private Double _answer;

    /**
     * Message from a sender asking for the grade of interest [0-1]
     * the receiver has on the given Product
     *
     * Note: Auction house should verify the "correctness" of the sender.
     *
     * @param sender the sender of this message
     * @param receiver  the receiver of this message
     * @param product the product we are asking about
     */
    public Missatge(String sender, String receiver, Producte product) {
        _sender = sender;
        _receiver = receiver;
        _product = product;
        _answer = -1.0;
    }

    public String getReceiver() {
        return _receiver;
    }

    public void setReceiver(String _receiver) {
        this._receiver = _receiver;
    }

    public String getSender() {
        return _sender;
    }

    public void setSender(String _sender) {
        this._sender = _sender;
    }

    public Producte getProduct() {
        return _product;
    }

    public void setProduct(Producte _product) {
        this._product = _product;
    }

    public Double getAnswer() {
        return _answer;
    }

    public void setAnswer(Double _answer) {
        this._answer = _answer;
    }
}
