import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Cris, Laura i Lluis on 17/11/2014.
 */
public class CasaSobreTancat extends AbstractCasa {

    private Map<String, Double> _offers;

    public CasaSobreTancat(Collection<Agent> agents, ArrayList<String> productesPanera) {
        super(SUBHASTA_SOBRETANCAT,agents,  productesPanera);
        _offers = new HashMap<>();
    }

    @Override
    public ResultatSubhasta ferSubhastaProducte(Producte producte) {
        // Inici del torn de converses entre agents
        super._realitzaTornPreguntes(producte);

        // Preguntem a cada agent per la seva oferta
        ExecutorService executor = Executors.newSingleThreadExecutor();
        List<Agent> agents = new ArrayList<>(_agents.values());
        Collections.shuffle(agents);
        
        String bestAgent = null;
        
        for (Agent agent: agents) {

            Future<Object> future = executor.submit(new DemanaOfertaTask(agent,producte));
            double oferta;
            try {
                oferta = (Double) future.get(TIMEOUT_OFERTA, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                oferta = -1;
            } catch (ExecutionException e) {
                oferta = -1;
            } catch (TimeoutException e) {
                oferta = -1;
            }

            // actualitzem el millor postor (bestAgent)
            if(bestAgent == null || oferta > _offers.get(bestAgent)) {
            	
            	// Comprovem que l'agent tingui el pressupost per pagar l'oferta que ha fet
            	if(_pressupost.get(agent._nom) >= oferta && oferta >= producte.get_preu())
            		bestAgent = agent._nom;
            	else
            		// Si no ha fet una oferta valida pel seu pressupost considerem oferta de -1
            		oferta = -1;
            } 
            _offers.put(agent._nom, oferta);
        }

        executor.shutdown();

        // Actualitzem el preu del producte amb l'oferta del guanyador
        if(bestAgent != null) {
            producte.set_preu(_offers.get(bestAgent));
        }

        return new ResultatSubhasta(bestAgent, producte, _offers);
    }

}
