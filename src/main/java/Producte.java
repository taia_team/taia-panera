import java.util.Objects;
import java.util.Vector;

/**
 * Created by Marcos on 17/11/2014.
 */
public class Producte {
    private String _nom;
    private double _preu;
    private String _tipus;
    private Vector<Integer> _atributs;
    
    public static final int AHORRO = 0;
    public static final int CALIDAD = 1;
    public static final int CAPACIDAD_ECONOMICA = 2;
    public static final int DESAYUNO = 3;
    public static final int DISPONIBILIDAD = 4;
    public static final int FRECUENCIA = 5;
    public static final int MARCA = 6;
    public static final int NECESIDAD = 7;
    public static final int NOVEDAD = 8;
    public static final int PRACTICO = 9;
    public static final int SANO = 10;
    public static final int SATISFACCION = 11;
    public static final int KEY = 12;


    /**
     * Constructor per defecte
     */
    public Producte() {
        this._nom = "";
        this._preu = 0;
        this._tipus = "";
        this._atributs = null;
    }

    /**
     * Constructor amb paràmetres
     * @param _nom no buit
     * @param _preu > 0
     * @param _tipus no buit
     * @param attr 
     */
    public Producte(String _nom, double _preu, String _tipus, Vector<Integer> attr) {
        this._nom = _nom;
        this._preu = _preu;
        this._tipus = _tipus;
        this._atributs = new Vector<>(attr);
    }

    public Producte(Producte p) {
        _nom = p._nom;
        _preu = p._preu;
        _tipus = p._tipus;
        _atributs = p._atributs;
    }


    public Producte copiaProducte(){
        return new Producte(this._nom,this._preu,this._tipus,this._atributs);
   }

    public String get_nom() {
        return _nom;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }

    public double get_preu() {
        return _preu;
    }

    public void set_preu(double _preu) {
        this._preu = _preu;
    }

    public String get_tipus() {
        return _tipus;
    }

    public void set_tipus(String _tipus) {
        this._tipus = _tipus;
    }

    public int get_Atribut(int atr){
	    return _atributs.elementAt(atr);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Producte) {
            Producte p = (Producte) o;
            return _nom.equals(p._nom);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return _nom.hashCode();
    }
}
