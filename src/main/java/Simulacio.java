import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * Created by LLUIS on 25/11/2014.
 */
public class Simulacio {

    protected List<Agent> _agents;
    protected List<Producte> _productes;
    protected List<AbstractCasa> _cases;

    public Simulacio(List<Agent> agents, List<Producte> productes, ArrayList<String> productesPanera){
        _agents = agents;
        _productes = productes;
        _cases = new ArrayList<>();
        _cases.add(new CasaAnglesa(_agents, productesPanera));
        _cases.add(new CasaHolandesa(_agents, productesPanera));
        _cases.add(new CasaSobreTancat(_agents, productesPanera));
        _cases.add(new CasaSobreTancatv2(_agents, productesPanera));
    }


    /**
     * Mètode per obtenir la llista de productes
     */
    private static List<Producte> obtenirProductes(String filePath) {
        //@author Lluis
        List<Producte> productes = new ArrayList<>();
        try
        {
            FileInputStream file = new FileInputStream(new File(filePath));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next(); // Saltem la fila dels "titols" de les columnes
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                // Cal seguir l'ordre dels atributs dins l'excel
                String tipus = cellIterator.next().getStringCellValue();        // Llegir tipus
                String nom = cellIterator.next().getStringCellValue();          // Llegir nom
                Vector<Integer> aux = new Vector<>();
                for(int i = 0; i < 13 ; i++){
                	aux.add((int) cellIterator.next().getNumericCellValue());   // Llegir atributs + key
                }
                double preu = cellIterator.next().getNumericCellValue();        // Llegir preu
                Producte p = new Producte(nom,preu,tipus,aux);
                productes.add(p);
            }
            file.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return productes;
    }

    /**
     * Metode per obtenir els agents que participaran en la casa
     */
    private static List<Agent> obtenirAgents(ArrayList<String> productesPanera) {
        //@todo implementacio per obtenir els agents

        List<Agent> agents = new ArrayList<>();

        // Afegir aqui els agents


        // Anunciem a cada agent qui seran els seus competidors
        List<String> competidors = new ArrayList<>(agents.size());
        for (Agent agent: agents) {
            competidors.add(agent._nom);
        }
        for (Agent agent: agents) {
            List<String> copia = new ArrayList<>(competidors);
            agent.rebreCompetidors(copia);
        }

        return agents;
    }

    public List<AbstractCasa> getCases(){
        return _cases;
    }

    public List<Producte> getProductes(){
        return _productes;
    }

    public static void main(String[] args){

        ArrayList<String> productesPanera = new ArrayList<>();
        // Afegir aqui els productes que es volen a la panera

        List<Agent> agents = obtenirAgents(productesPanera);
        List<Producte> productes = obtenirProductes("src/main/resources/cataleg.xlsx");

        Simulacio sim = new Simulacio(agents, productes, productesPanera);
        for(AbstractCasa casa: sim.getCases()){

            // En principi subhastem tots els productes del simulador, si cal ja es canviara
            casa.ferSubhasta(sim.getProductes());

        }
    }
}
