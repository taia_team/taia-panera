import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Kevin on 18/11/2014.
 */
public class CasaSobreTancatv2 extends AbstractCasa {

    private Map<String, Double> _offers;

    public CasaSobreTancatv2(Collection<Agent> agents, ArrayList<String> productesPanera) {
        super(SUBHASTA_SOBRETANCAT_2,agents, productesPanera);
        _offers = new HashMap<>();
    }

    @Override
    public ResultatSubhasta ferSubhastaProducte(Producte producte) {
        // Inici del torn de converses entre agents
        super._realitzaTornPreguntes(producte);

        // Preguntem a cada agent per la seva oferta
        ExecutorService executor = Executors.newSingleThreadExecutor();
        List<Agent> agents = new ArrayList<>(_agents.values());
        Collections.shuffle(agents);
        
        _offers.clear();
        
        String bestAgent = null;
        String secondAgent = null;
        
        for (Agent agent: agents) {

            Future<Object> future = executor.submit(new DemanaOfertaTask(agent,producte));
            double oferta;
            try {
                oferta = (Double) future.get(TIMEOUT_OFERTA, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                oferta = -1;
            } catch (ExecutionException e) {
                oferta = -1;
            } catch (TimeoutException e) {
                oferta = -1;
            }
            
            if(bestAgent == null || oferta > _offers.get(bestAgent)) {
            	
            	// Comprovem que l'agent tingui el pressupost per pagar l'oferta que ha fet
            	if(_pressupost.get(agent._nom) >= oferta && oferta >= producte.get_preu()) {
            		secondAgent = bestAgent;
            		bestAgent = agent._nom;
            	} else {
            		// Si no ha fet una oferta valida pel seu pressupost considerem oferta de -1
            		oferta = -1;
            	}
            } 
            _offers.put(agent._nom, oferta);
        }

        executor.shutdown();

        if(secondAgent != null) {
        	producte.set_preu(_offers.get(secondAgent));
        } 

        return new ResultatSubhasta(bestAgent, producte, _offers);
    }
}
