import sun.plugin.dom.exception.InvalidStateException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Marcos on 17/11/2014.
 */
public abstract class Agent {

    private final static double COST_PREGUNTA = 1.0;

    protected String _nom;
    protected Double _pressupost;
    protected ArrayList<Producte> _productes;
    protected ArrayList<String> _agentsCompetidors;
    protected List<String> _tipusProductesPanera;
    protected Map<Integer, Integer> _profiling;

    public Agent(ArrayList<String> tipusProductes) {
        _agentsCompetidors = null; // El simulador s'encarregara de passar els competidors
        _tipusProductesPanera = tipusProductes;
        _pressupost = AbstractCasa.PRESSUPOST_INICIAL;
        _nom = this.getClass().getName();
        definirProfiling();
    }

    /**
     * Funció on cada agent definira el seu _profiling
     */
    protected abstract void definirProfiling();

    protected abstract List<Missatge> enviarPreguntes(Producte producte);

    public final List<Missatge> $enviarPreguntes(Producte producte) {
        List<Missatge> preguntes = enviarPreguntes(producte);
        if (preguntes == null)
            return new ArrayList<>();

        _pressupost -= COST_PREGUNTA*preguntes.size();

        // Si no te diners per fer tantes preguntes, anul·lem el request
        if (_pressupost < 0) {
            _pressupost += COST_PREGUNTA*preguntes.size();
            preguntes.clear();
        }

        return preguntes;
    }

    public void rebreCompetidors(Collection<String> competidors) {
        _agentsCompetidors = new ArrayList<>(competidors);
    }

    public void rebreProductes(Collection<Producte> productes) {
        _productes = new ArrayList<>(productes);
    }

    protected abstract Missatge rebrePreguntes(Missatge missatge);

    public final Missatge $rebrePreguntes(Missatge missatge) {
        _pressupost += COST_PREGUNTA;
        return rebrePreguntes(missatge);
    }

    public abstract void procesarRespostes(List<Missatge> respostes);

    public abstract Boolean rebreSubhastaHolandesa(Producte producte);

    public abstract Double rebreSubhastaTancat(Producte producte);

    public abstract Double rebreSubhastaTancat_2(Producte producte);

    public abstract Double rebreSubhastaAnglesa(Producte producte);

    public abstract boolean rebreDadesResultatSubasta(ResultatSubhasta resultatSubhasta);

    //public abstract boolean rebreGuanyadorSubasta();

    public abstract void rebreNovaPujaSubhastaAnglesa(Producte producte, String pujadorActual);

    public final double calcularMaxRecall() {

        if (_tipusProductesPanera.isEmpty())
            throw new IllegalStateException("_tipusProductes.isEmpty()");

        List<Double> maximsr = new ArrayList<>();

        // Per cada tipus de producte que hagi d'apareixer a la panera
        for (String tipus : _tipusProductesPanera) {
            double max = -1;
            for (Producte producte : _productes) {
                if (producte.get_tipus().equals(tipus)) {
                    double recall = calcularRecallProducte(producte);
                    max = recall > max ? recall : max;
                }
            }
            if (max == -1)
                throw new IllegalStateException("max == -1");

            maximsr.add(max);
        }

        // De moment
        double sum = 0;
        for (Double recall : maximsr) {
            sum += recall;
        }

        return sum;
    }

    /**
     * Funcio per calcular el recall a partir d'un producte
     * @param p
     * @return retorn
     */
    final public double calcularRecallProducte(Producte p) {
        double retorn = 0;

        for(Map.Entry<Integer, Integer> categoria : _profiling.entrySet()) {
            if (p.get_Atribut(categoria.getKey()) == categoria.getValue()) retorn++;
        }
        return retorn/(double)_profiling.size();
    }
}