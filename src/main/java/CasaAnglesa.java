import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CasaAnglesa extends AbstractCasa{

	public CasaAnglesa(Collection<Agent> agents, ArrayList<String> productesPanera) {
        super(SUBHASTA_ANGLESA,agents, productesPanera);
    }

	@Override
	public ResultatSubhasta ferSubhastaProducte(Producte producte) {
		Random rn = new Random();
		int rang = 80 - 20 + 1;
		int randomNum =  rn.nextInt(rang) + 20;
		double aleatori = (double)(randomNum * 0.01);
        double preu_inicial = (double) producte.get_preu() * aleatori;
        //if (preu_inicial > AbstractCasa.PRESSUPOST_INICIAL) preu_inicial = AbstractCasa.PRESSUPOST_INICIAL;
        Producte p_aux = producte.copiaProducte();
        p_aux.set_preu(preu_inicial);
        List<Agent> agents = new ArrayList<Agent>(_agents.values());
        Map<String, Double> ofertes = new HashMap<String,Double>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        String a_guanyador = null;
        int counter=0;
        double max=0.0;
        for (Agent agent : agents) {
        	ofertes.put(agent._nom, 0.0);
        }
        super._realitzaTornPreguntes(p_aux);
        while (counter<3) {
           
            Collections.shuffle(agents);
            for (Agent agent : agents) {
                Future<Object> future = executor.submit(new DemanaOfertaTask(agent,p_aux));
                double oferta_actual=0.0;
                try {
                	oferta_actual = (Double) future.get(TIMEOUT_OFERTA, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                	oferta_actual = -1;
                } catch (ExecutionException e) {
                	oferta_actual = -1;
                } catch (TimeoutException e) {
                	oferta_actual = -1;
                }

                if(oferta_actual > max && agent._pressupost >= oferta_actual){
                	max=oferta_actual;
                	p_aux.set_preu(oferta_actual);
                	a_guanyador = agent._nom;
                	counter = 0;
                	ofertes.put(agent._nom,oferta_actual);
                	novaPujaSubhastaAnglesa(p_aux,a_guanyador);
                	break;
                }
            }
            counter++;
        }

        executor.shutdown();
        
        ResultatSubhasta resultat = new ResultatSubhasta(a_guanyador,p_aux,ofertes);
        
        return resultat;
	}

	private void novaPujaSubhastaAnglesa(Producte producte, String pujadorActual) {
		// TODO Auto-generated method stub
		List<Agent> agents = new ArrayList<Agent>(_agents.values());
		for(Agent agent: agents){
			agent.rebreNovaPujaSubhastaAnglesa(producte,pujadorActual);
		}
		
	}


}
