import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Marcos on 17/11/2014.
 * Classe per la casa de subhastes. Implementa les funcions basiques i deixa oberta la implementació
 * de la resta de funcions necessaries de cada casa.
 * Pseudocodi general del funcionament de cada casa de subhastes:
 * 
 * Crear casa de subhastes
 * Obtenir llista productes aleatoris
 * Per cada producte aleatori
 *      Anunciar producte subhasta
 *      Iniciar subhasta producte
 *          (Manteniment de preguntes)
 *          (Iteracio o no de pasos implementat per cada casa)
 *      Anunciar guanyador
 *      Anunciar resultats subhasta
 * Fi per
 * 
 */
public abstract class AbstractCasa {

    protected static final int SUBHASTA_ANGLESA = 0;
    protected static final int SUBHASTA_HOLANDESA = 1;
    protected static final int SUBHASTA_SOBRETANCAT = 2;
    protected static final int SUBHASTA_SOBRETANCAT_2 = 3;

    public static final int TIMEOUT_OFERTA = 3;      // in seconds
    public static final int TIMEOUT_CONVERSA = 1;    // in seconds

    public static final double PRESSUPOST_INICIAL = 1000.0;


    protected Map<String, Agent> _agents;
    protected Map<String, Double> _pressupost; /**<  Per cada agent, guardem el seu pressupost actual */
    protected List<Producte> _productes;
    protected List<String> _productesPanera; /**< Llista dels tipus de productes que ha de contenir la panera */
    protected HashMap<String, List<Producte>> _productesComprats;


    protected final int _tipusCasa;


    /**
     * Metode per inicialitzar els atributs de la casa de subhasta
     */
    public AbstractCasa(int tipusCasa, Collection<Agent> agents, List<String> productesPanera){

        _productesComprats = new HashMap<>();

        // Inicialitzem map d'agents
        _agents = new HashMap<>(2*agents.size());
        for (Agent agent: agents) {
            _agents.put(agent._nom, agent);
            _productesComprats.put(agent._nom, new ArrayList<Producte>());
        }

        _productesPanera = productesPanera;
        _pressupost = new HashMap<>(2*_agents.size());
        _tipusCasa = tipusCasa;

    }


    private void _inicialitzarPressupostos() {
        // Inicialitzem el pressupost de cada agent
        for (String agentName: _agents.keySet()) {
            _pressupost.put(agentName, PRESSUPOST_INICIAL);
        }
    }


    /**
     * Metode per obtenir una llista aleatoria de productes
     *
     * @param nElements sempre > 0
     * @return retorn - llista de productes escollits aleatoriament
     */
    protected List<Producte> obtenirLlistaProductesAleatoris(Integer nElements) {
        List<Producte> retorn = this._productes.subList(0,this._productes.size()-1);
        long seed = System.nanoTime();
        Collections.shuffle(retorn, new Random(seed));
        retorn = retorn.subList(0, nElements);
        return retorn;
    }

    /**
     * Metode per avisar als Agents dels productes que es subhastaran
     */
    protected void anunciarLlistaProductes(){

        if (_productes == null)
            throw new IllegalStateException("_productes == null");

        for(Agent agent : _agents.values()) {
            List<Producte> copia = new ArrayList<>(_productes);
            agent.rebreProductes(copia);
        }
    }

    /**
     * Metode per avisar als Agents del producte actual que es subhastara
     */
    private void anunciarProducte(Producte producte){
        //@todo implementacio per anunciar el producte


    }

    public final void ferSubhasta(List<Producte> productes) {
        _productes = productes;
        _inicialitzarPressupostos();
        anunciarLlistaProductes();
        for (Producte producte: productes) {
            ResultatSubhasta resultat = ferSubhastaProducte(new Producte(producte));
            anunciarDadesResultatsSubhasta(resultat);
        }
        _calcularResultatFinal();
    }

    /**
     * Metode per fer la subhasta d'un producte. Aquest metode depen del tipus de casa i del seu funcionament.
     * La casa interactua amb els agents fins a obtenir un guanyador pel producte.
     * Coses que es fan en aquesta funcio:
     *      - Processar preguntes
     *      - Processar respostes
     *      - Fer la subhasta (tractament iteratiu o no d'oferir el producte o obtenir puges)
     * Coses que NO es fan en aquesta funcio:
     *      - Anunciar el producte
     *      - Anunciar guanyadors o resultats de la subhasta
     * Cal que cada casa utilitzi els metodes propis per a la seva subhasta, i cal que els agents tambe utilitzin
     * els metodes que necessiti per poder treballar amb la casa
     * @param producte NO HA DE TENIR DADES DE PREU
     * @return nom del guanyador de la subhasta
     */
    public abstract ResultatSubhasta ferSubhastaProducte(Producte producte);


    /**
     * Metode per anunciar als Agents de les dades finals del resultat de la subhasta. El tipus de dades poden variar
     * segons el tipus de casa.
     */
    public void anunciarDadesResultatsSubhasta(ResultatSubhasta resultat){

        ExecutorService executor = Executors.newSingleThreadExecutor();

        for (Agent agent: _agents.values()) {
            Future<Boolean> future = executor.submit(new EnviaResultatsSubhastaTask(agent, resultat));
            try {
                future.get(TIMEOUT_CONVERSA, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                System.err.println("Error: anunciarDadesResultatsSubhasta(): Agent = "+ agent._nom);
                System.out.println(e);
            }
        }

        executor.shutdown();

        // actualitzem pressupost del guanyador, si n'hi ha
        if (resultat.guanyador != null) {
            // Ull! s'assumeix que el preu que ha de pagar el guanyador es el preu del producte!
            _pressupost.put(resultat.guanyador, _pressupost.get(resultat.guanyador) - resultat.producte.get_preu());

            // Actualitzem la llista de productes comprats de l'agent guanyador
            // Assegurant que nomes contingui com a molt un producte de cada tipus!! (el millor)
            List<Producte> productesAgent = _productesComprats.get(resultat.guanyador);
            Agent winner = _agents.get(resultat.guanyador);
            for (Producte producte: productesAgent) {
                if (producte.get_tipus().equals(resultat.producte.get_tipus())) {
                    // Si havia comprat un producte del mateix tipus i l'actual es millor, eliminem l'anterior
                    if (winner.calcularRecallProducte(producte) < winner.calcularRecallProducte(resultat.producte)) {
                        productesAgent.remove(producte);
                        productesAgent.add(resultat.producte);
                    }

                    // Si ja havia comprat un producte d'aquest tipus, retornem
                    return;
                }
            }
            // Si no havia comprat cap producte d'aquest tipus, l'afegim.
            productesAgent.add(resultat.producte);
        }
    }

    private void _calcularResultatFinal() {
        // Primera versio, sense tenir en compte el _profiling
    	
    	for(Agent agent: _agents.values()) {

            System.out.println("PUNTUACIO DE "+agent._nom);

    		List<String> copiaProductesPanera = new ArrayList<String>(_productesPanera);
    		int recallPanera = 0;
            double recallProfiling = 0;
    		for(Producte p : _productesComprats.get(agent._nom)) {
    			if(copiaProductesPanera.contains(p.get_tipus())) {
    				recallPanera++;
                    recallProfiling += agent.calcularRecallProducte(p);
                    copiaProductesPanera.remove(p.get_tipus());
                }
                System.out.println(p.get_nom()+"\t"+p.get_preu());
            }

            double puntuacioRecallPanera = recallPanera/(double)_productesPanera.size();
            double puntuacioRecallProfiling = recallProfiling/(agent.calcularMaxRecall());
            double puntuacioPressupost = _pressupost.get(agent._nom)/AbstractCasa.PRESSUPOST_INICIAL;

    		double puntuacio = 0.65*puntuacioRecallPanera+0.25*puntuacioRecallProfiling+0.1*puntuacioPressupost;

            System.out.println();
    		System.out.println("L'agent "+agent._nom+" ha comprat "+recallPanera+" productes dels "+_productesPanera.size()+" que es demanaven, "+
    		"i li ha sobrat un pressupost final de "+_pressupost.get(agent._nom)+" euros. Per tant la seva puntuacio final es de "+ puntuacio +" punts.");

    	}
        // Calculem la pasta feta
        double total = 0.0;
        for (String agent: _agents.keySet()) {
            total += _pressupost.get(agent);
        }
        System.out.println("En total s'ha recaudat: "+((_agents.size()*PRESSUPOST_INICIAL)-total));
    }
    
    protected void _realitzaTornPreguntes(Producte producte) {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        // 1. Demanem a cada agent quines preguntes vol enviar
        List<Missatge> preguntes = new LinkedList<>();
        for (Agent agent: _agents.values()) {
            Future<List<Missatge>> future = executor.submit(new DemanaPreguntesTask(agent, producte));

            List<Missatge> missatges = new LinkedList<>();
            try {
                missatges.addAll(future.get(TIMEOUT_CONVERSA, TimeUnit.SECONDS));
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                //
            }

            _afegeixMissatgesValids(agent, preguntes, missatges);
        }

        // 2. Enviem a cada agent les preguntes que li han fet
        Map<String, List<Missatge>> respostes = new HashMap<>();
        for (Missatge missatge: preguntes) {

            missatge.setAnswer(Missatge.SENSE_RESPOSTA);
            Agent receiver = _agents.get(missatge.getReceiver());
            Future<Missatge> future = executor.submit(new DemanaRespostaTask(receiver, missatge));

            // Si pel motiu que sigui, l'agent no respon la resposta tindra valor SENSE_RESPOSTA
            Missatge resposta = missatge;
            try {
                resposta = future.get(TIMEOUT_CONVERSA, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                //
            }

            // Si l'entrada corresponent a l'agent no existia, li creem la llista de missatges
            if (!respostes.containsKey(resposta.getSender()))
                respostes.put(resposta.getSender(), new LinkedList<Missatge>());

            respostes.get(resposta.getSender()).add(resposta);
        }

        // 3. Enviem a cada agent la resposta a les preguntes que ha fet
        for (Agent agent : _agents.values()) {
            Future<Boolean> future = executor.submit(new EnviaRespostesTask(agent, respostes.get(agent._nom)));

            try {
                future.get(TIMEOUT_CONVERSA, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }

            // Per debuggar es podria mirar quantes d'aquestes crides retornen cert...
        }

        executor.shutdown();

        // 4. Apliquem els costos i els guanys de cada pregunta
        for (Missatge missatge: preguntes) {
            _pressupost.put(missatge.getSender(),_pressupost.get(missatge.getSender())-1.0);
            _pressupost.put(missatge.getReceiver(),_pressupost.get(missatge.getReceiver())+1.0);
        }

    }

    private void _afegeixMissatgesValids(Agent sender, List<Missatge> preguntes, List<Missatge> llista) {
        // per cada missatge de la llista, comprovem que sigui valid
        for (Missatge missatge: llista) {
            // comprovem que no hi hagin camps clau nuls o que el missatge no vagi dirigit al mateix que l'envia
            if (missatge.getSender() == null || missatge.getProduct() == null ||
                    missatge.getReceiver() == null || missatge.getSender().equals(missatge.getReceiver()))
                continue;

            // Comprovem que el missatge te com a remitent l'agent que l'ha enviat
            if (sender._nom.equals(missatge.getSender()) && _agents.containsKey(missatge.getReceiver()))
                preguntes.add(missatge);
        }
    }

    /**
     * Implementacio classes auxiliars per a l'execucio 'segura' de rutines implementades pels agents
     */

    class EnviaRespostesTask implements Callable<Boolean> {

        private Agent _agent;
        private List<Missatge> _respostes;

        EnviaRespostesTask(Agent agent, List<Missatge> respostes) {
            _agent = agent;
            _respostes = respostes;
            if (respostes == null)
                _respostes = new ArrayList<>();
        }

        @Override
        public Boolean call() throws Exception {
            _agent.procesarRespostes(_respostes);
            return true;
        }
    }

    class DemanaRespostaTask implements Callable<Missatge> {

        private Agent _agent;
        private Missatge _missatge;

        DemanaRespostaTask(Agent agent, Missatge missatge) {
            _agent = agent;
            _missatge = missatge;
        }

        @Override
        public Missatge call() throws Exception {
            return _agent.$rebrePreguntes(_missatge);
        }
    }

    class DemanaOfertaTask implements Callable<Object> {

        private Agent _agent;
        private Producte _producte;

        DemanaOfertaTask(Agent agent, Producte producte) {
            _agent = agent;
            _producte = producte;
        }

        @Override
        public Object call() throws Exception {
            switch(_tipusCasa) {
                case SUBHASTA_ANGLESA:
                    return _agent.rebreSubhastaAnglesa(_producte);
                case SUBHASTA_HOLANDESA:
                    return _agent.rebreSubhastaHolandesa(_producte);
                case SUBHASTA_SOBRETANCAT:
                    return _agent.rebreSubhastaTancat(_producte);
                case SUBHASTA_SOBRETANCAT_2:
                    return _agent.rebreSubhastaTancat_2(_producte);
                default:
                    throw new IllegalStateException("Invalid _tipusCasa");
            }
        }
    }

    class DemanaPreguntesTask implements Callable<List<Missatge>> {

        private Agent _agent;
        private Producte _producte;

        DemanaPreguntesTask(Agent agent, Producte producte) {
            _agent = agent;
            _producte = producte;
        }

        @Override
        public List<Missatge> call() throws Exception {
            return _agent.$enviarPreguntes(_producte);
        }
    }

    class EnviaResultatsSubhastaTask implements Callable<Boolean> {

        private ResultatSubhasta resultatSubhasta;
        private Agent agent;

        EnviaResultatsSubhastaTask(Agent agent, ResultatSubhasta resultatSubhasta) {
            this.agent = agent;
            this.resultatSubhasta = resultatSubhasta;
        }

        @Override
        public Boolean call() throws Exception {
            return agent.rebreDadesResultatSubasta(resultatSubhasta);
        }
    }

    public List<Producte> getProductes(){
        return _productes;
    }
}
