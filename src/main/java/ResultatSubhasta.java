
import java.util.Map;

/**
 *
 * @author aula
 */
public class ResultatSubhasta {
    public String guanyador;
    public Producte producte;
    public Map<String, Double> ofertes;
    
    public ResultatSubhasta(String guanyador, Producte producte, Map<String, Double> ofertes) {
        this.guanyador = guanyador;
        this.producte = producte;
        this.ofertes = ofertes;
    }

	public String getGuanyador() {
		return guanyador;
	}

	public void setGuanyador(String guanyador) {
		this.guanyador = guanyador;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public Map<String, Double> getOfertes() {
		return ofertes;
	}

	public void setOfertes(Map<String, Double> ofertes) {
		this.ofertes = ofertes;
	}
}
