import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Marcos on 18/11/2014.
 */
public class CasaHolandesa extends AbstractCasa {

    public CasaHolandesa(Collection<Agent> agents,ArrayList<String> productesPanera) {
        super(SUBHASTA_HOLANDESA, agents, productesPanera);
    }

    @Override
    public ResultatSubhasta ferSubhastaProducte(Producte producte) {
        //Iniciem el preu del producte
        Random generator = new Random();
        double valor = generator.nextDouble() * 0.85;
        if (valor<0.25) valor = 0.25;
        Double preu_minim = producte.get_preu() * valor;
        Double preu_inicial = producte.get_preu() * 10;
        if (preu_inicial > AbstractCasa.PRESSUPOST_INICIAL) preu_inicial = AbstractCasa.PRESSUPOST_INICIAL;

        Producte p_aux = producte.copiaProducte();
        p_aux.set_preu(preu_inicial);

        List<Agent> agents = new ArrayList<Agent>(_agents.values());

        ExecutorService executor = Executors.newSingleThreadExecutor();

        boolean fi = p_aux.get_preu() < preu_minim;
        boolean r_subasta = false;
        Map<String, Double> a_ofertes = new HashMap<>();
        String a_guanyador = null;
        Double a_preu = 0D;
        while (!fi) {
            super._realitzaTornPreguntes(p_aux);
            Collections.shuffle(agents);
            for (Agent a : agents) {
                try {
                    Future<Object> future = executor.submit(new DemanaOfertaTask(a, p_aux));
                    try {
                        r_subasta = (Boolean) future.get(TIMEOUT_OFERTA, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        r_subasta = false;
                    } catch (ExecutionException e) {
                        r_subasta = false;
                    } catch (TimeoutException e) {
                        // Potser hauriem de fer alguna cosa mes drastica si s'ha esgotat el timeout...
                        r_subasta = false;
                    }

                    if (r_subasta && a._pressupost >= p_aux.get_preu()) {
                        a_guanyador = a._nom;
                        a_preu = p_aux.get_preu();
                        break;
                    }
                } catch (Exception e) {
                    //res, pasem d'aquest agent
                }
            }
            fi = r_subasta || p_aux.get_preu() < preu_minim;
            if (!fi) {
                p_aux.set_preu(p_aux.get_preu() * 0.90);
            }
        }

        executor.shutdown();

        for (Map.Entry<String, Agent> pagents : this._agents.entrySet()) {
            a_ofertes.put(pagents.getKey(),0D);
        }

        if (a_guanyador != null) a_ofertes.put(a_guanyador,a_preu);


        return new ResultatSubhasta(a_guanyador,p_aux,a_ofertes);
    }
}
